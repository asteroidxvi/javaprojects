package matrix_multiplication;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;
import java.util.concurrent.RecursiveAction;

public class MatrixMultiplicationForkJoin extends RecursiveTask<double[][]>{

		private static BufferedReader inputfileName; //read from file passed as 1st Argo
		private static FileOutputStream outputFile;
		private static double[][] aMatrix;
		private static double[][] bMatrix;
		private static double[][] c;
		static long ts;
		static long tf;
		static int MATRIXSIZE, rowSize, colSize;
		private static final int task_threshold = 10;
		public MatrixMultiplicationForkJoin(double[][] aM, double[][] bM, int size)
		{
			this.aMatrix = aM; this.bMatrix = bM; this.MATRIXSIZE = size;
		}
		protected double[][] compute()
		{
			if(MATRIXSIZE <= task_threshold)//if matrix is less than 10x10 recurse
			{
				return c = multiply(aMatrix, bMatrix);
			}
			else
			{
				int mid = MATRIXSIZE/2; //create two new smaller matrixes
				int n = MATRIXSIZE;
				double[][] newAM1 = new double[mid][n];
				for(int i =0; i < mid; i++)
					for(int j =0; j < n; j++)
						newAM1[i][j] = aMatrix[i][j];
				double[][] newBM1 = new double[mid][n];
				for(int i =0; i < mid; i++)
					for(int j =0; j < n; j++)
						newBM1[i][j] = bMatrix[i][j];
				double[][] newAM2 = new double[mid][n];
				for(int i =mid; i < n; i++)
					for(int j =0; j < n; j++)
						newAM2[i][j] = aMatrix[i][j];
				double[][] newBM2 = new double[mid][n];
				for(int i =mid; i < n; i++)
					for(int j =0; j < n; j++)
						newBM2[i][j] = bMatrix[i][j];
				
				MatrixMultiplicationForkJoin left = new MatrixMultiplicationForkJoin(newAM1, newBM1, mid);
				MatrixMultiplicationForkJoin right = new MatrixMultiplicationForkJoin(newAM2, newBM2, mid);
				left.fork();
				right.fork();
				double[][] c1 = left.join();
				double[][] c2 = right.join();
				for(int i =0; i < mid; i++)
					for(int j =0; j < n; j++)
						c[i][j] = c1[i][j];
				for(int i =mid; i < n; i++)
					for(int j =0; j < n; j++)
						c[i][j] = c2[i][j];
				return c;
			}
			
		}
		public static double[][] multiply(double[][] A, double[][] B)
		{
			int nA = A.length;
			int nB = B.length;
			int mA = A[0].length;
			int mB = B[0].length;
			if(nA != mB)
				throw new RuntimeException("incorrect matric values");
			
			double[][] outPutMatrix = new double[mA][nB];
			for(int i =0; i < mA; i++)
				for(int j =0; j < nB; j++)
					for(int k =0; k<nA; k++)
						outPutMatrix[i][j] += (A[i][k]*B[k][j]);
			return outPutMatrix;
		}
		private static void readInputFromFile() throws IOException
		{
			
			String read;
			int lineIncrement = 0;
			boolean done = false;
			boolean matrixB = false;
			while ((read = inputfileName.readLine()) != null)
			{
				StringTokenizer s = new StringTokenizer(read);//read entire line into s
				MATRIXSIZE = s.countTokens();//count number of elements in the line saved in s
				rowSize = MATRIXSIZE;
				colSize = MATRIXSIZE;
				int n = MATRIXSIZE;
				if(!done)//create new matrix 1st time around
				{
					aMatrix = new double[n][n];
					bMatrix = new double[n][n];
					done = true;
				}
				if(n==0)//if no elements read, time to 
				{
					matrixB = true;
					lineIncrement = 0;
					continue;
				}
				int i = 0;
				while(s.hasMoreTokens() && (matrixB == false))
				{
					int a = Integer.parseInt(s.nextToken());
					aMatrix[lineIncrement][i] = a;
					i++;
				}
				int j = 0;
				while(s.hasMoreTokens() && (matrixB == true))
				{
					int a = Integer.parseInt(s.nextToken());
					bMatrix[lineIncrement][j] = a;
					j++;
				}
				lineIncrement++;
			}
		}
		private static void printInitialMatrix()
		{
			System.out.println("Matrix A provided is:");
			printMatrix(aMatrix); 
			System.out.println("Matrix B provided is:");
			printMatrix(bMatrix);
		}
		private static void printMatrix(double[][] c)
		{
//			boolean row3 = false;
			for(int i = 1; i <= c[0].length; i++)
			{
				System.out.print("\tCol "+i);

			}
			System.out.println();
	        for ( int i = 0 ; i < c.length ; i++ )
	        {
	        	System.out.print(" Row "+(i+1)+" \t");
	           for ( int j = 0 ; j < c[0].length ; j++ )
	           {
	              System.out.print(c[i][j]+"\t");
	           }
	           System.out.println();
	        }
		}
		private static void writeOutputToFile() throws IOException
		{
			File file = new File("output_file_ForkJoinPoolRecursiveAction");
			outputFile = new FileOutputStream(file);
			if(!file.exists())
			{
				file.createNewFile();
			}
			writeFile("MATRIX MULT RESULT:\n");
			for(int i = 1; i <= c[0].length; i++)
			{
				writeFile("\tCol "+i);
			}
			writeFile("\n");
	        for ( int i = 0 ; i < c.length ; i++ )
	        {
	        	writeFile(" Row "+(i+1)+" \t");
	           for ( int j = 0 ; j < c[0].length ; j++ )
	           {
	        	   	double value = c[i][j];
	        	   	String inwa = String.valueOf(value)+"\t";
	        	   	writeFile(inwa);
	           }
	           writeFile("\n");
	   		}
	        System.out.println("Written output to file: output_file_ForkJoinPoolRecursiveAction");
		}
		private static void writeFile(String s) throws IOException
		{
			byte[] input = s.getBytes();
			outputFile.write(input);
			outputFile.flush();
		}
		public static void main(String[] args) throws IOException 
		{
			//read input from file and then parse the data into two 2-dimentional array for computing
			if(args.length == 0)
			{
				try//get the filename to read from and the number of threads to launch
				{
					inputfileName = new BufferedReader(new FileReader("input_file"));
					readInputFromFile();	
				}catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else if(args.length == 1)
			{
				try//get the filename to read from and the number of threads to launch
				{
					inputfileName = new BufferedReader(new FileReader(args[0]));
					readInputFromFile();
				}catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				aMatrix = new double[][]
						{
							{1, 2, 3, 5},
							{4, 5, 6, 6},
							{7, 8, 7, 5},
							{7, 3, 9, 3}
						};
				bMatrix = new double[][]
						{
							{1, 2, 3, 4},
							{4, 5, 3, 3},
							{7, 3, 9, 3},
							{7, 3, 9, 3}
						};
				MATRIXSIZE = 4;
				rowSize = 4;
				colSize = 4;
			}
			ts = System.currentTimeMillis();
			ForkJoinPool fjp = new ForkJoinPool();
			fjp.invoke(new MatrixMultiplicationForkJoin(aMatrix, bMatrix, MATRIXSIZE));
			tf = System.currentTimeMillis();
			double time = (tf-ts)/1000.;
			System.out.println("Time taken for this MatrixMult Computation: " + time);
			printMatrix(aMatrix);printMatrix(bMatrix);
			printMatrix(c);
//			if(MATRIXSIZE <= 10)//do not print output, save output to fine
//			{
//				printInitialMatrix();//output matrixA and B
//				System.out.println("Matrix Multiplication Results!\n");
//				printMatrix(c);
//				try{
//					writeOutputToFile();
//				}catch(Exception e)
//				{
//					e.printStackTrace();
//				}
//			}
//			else//large matrix, save output to a file
//			{
//				try{
//					writeOutputToFile();
//				}catch(Exception e)
//				{
//					e.printStackTrace();
//				}
//			}
//			outputFile.close();
		}
	}
