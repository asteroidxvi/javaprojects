/* openmp version of dot produce in C */
#include<stdio.h>
#include<stdlib.h>
#include<omp.h>

int main(int argc, char **argv)
{
  // if(argc == 0 || argc > 3)
  // {
  //   printf("Incorrect number of arguments, please provide 2 arguments\n");
  //   printf("Argument 1 is the inputfile\n");
  //   exit(0);
  // }

  // FILE *ofile = NULL;  //inputfile
  // int nt = atoi(argv[2]); //number of threads to spawn
  // size_t bufread = 0;
  // char *buffer = NULL;
  // ofile = fopen(argv[1], "r");
  // if(ofile == NULL)
  //   perror("Error openting file");
  // ssize_t read;
  // while((read == getline(&buffer, &bufread, ofile)) != -1)
  // {
  //   printf("got line %zu ", read);
  //   printf("%d", bufread);
  // }
  // free(bufread);

  // int row, col;
  // for(row = 0; row < )

  int i, j, k, ntmax;
  double angle;
  double ts, tf;
  int n = 500;

  ntmax = omp_get_max_threads();
  /* set number of threads to use */
  omp_set_num_threads(ntmax);
  printf("there are %d threads available\n\n", ntmax);

  //allocate the size of 3 2D arrays(2 for input and 1 with output of matrix mult)
  double **a = (double **) malloc(sizeof(double*)*n);
  double *aData = (double *) malloc(sizeof(double)*n*n);
  for(i = 0; i < n; i++)
  {
    a[i] = &aData[i*n];
  }
  double **b = (double **) malloc(sizeof(double*)*n);
  double *bData = (double *) malloc(sizeof(double)*n*n);
  for(i = 0; i < n; i++)
  {
    b[i] = &bData[i*n];
  }
  double **c = (double **) malloc(sizeof(double*)*n);
  double *cData = (double *) malloc(sizeof(double)*n*n);
  for(i = 0; i < n; i++)
  {
    c[i] = &cData[i*n];
  }
  //initial array from reading valeus from file
  double pi = 3.141592653589793;
  double s;
  int thread_num;
  double wtime;

  ts = omp_get_wtime();

  # pragma omp parallel shared ( a, b, c, n, pi, s ) private ( angle, i, j, k )
  {
    # pragma omp for
    for ( i = 0; i < n; i++ )
    {
      for ( j = 0; j < n; j++ )
      {
        angle = 2.0 * pi * i * j / ( double ) n;
        a[i][j] = s * ( sin ( angle ) + cos ( angle ) );
      }
    }

    # pragma omp for
    for ( i = 0; i < n; i++ )
    {
      for ( j = 0; j < n; j++ )
      {
        b[i][j] = a[i][j];
      }
    }

    # pragma omp for
    for ( i = 0; i < n; i++ )
    {
      for ( j = 0; j < n; j++ )
      {
        c[i][j] = 0.0;
        for ( k = 0; k < n; k++ )
        {
          c[i][j] = c[i][j] + a[i][k] * b[k][j];
        }
      }
    }

  }

  // class the matrix mult function
  // for(i = 0; i < n; i++)
  // {
  //   for(j =0; j <n; j++)
  //   {
  //     #pragma omp parallel for private(k) shared(a,b,n) reduction(+:**c) \
  //     schedule(static)
  //         for (k=0;k<n;k++)
  //             (**c) += ((**a)*(**b));
  //         // {
  //         //       // c[i][j] += (a[i][k]*b[i][j]);
  //         //       (**c) += ((**a)*(**b));
  //         // }
  //   }
  // }


  tf = omp_get_wtime();
  printf("dot product: time(s):%f\n", tf-ts);

  // fclose(ofile);
  free (a); free(b); free(c);
  free (aData); free(bData); free(cData);

  return 0;
}
