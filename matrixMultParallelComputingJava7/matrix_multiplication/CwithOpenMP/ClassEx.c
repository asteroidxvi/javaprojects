/* openmp version of dot produce in C */
#include<stdio.h>
#include<stdlib.h>
#include<omp.h>
 
int main(int argc, char **argv){
  int n = atoi(argv[1]);  /* array length */
  int nt = atoi(argv[2]);
 
  int i, ntmax;
  double dp = 0.0;
  double ts, tf;
 
  ntmax = omp_get_max_threads();
  /* set number of threads to use */
  omp_set_num_threads(nt);
 
  printf("there are %d threads available\n", ntmax);
 
  double *a = (double *) malloc(sizeof(double)*n);
  double *b = (double *) malloc(sizeof(double)*n);
 
  /* initialize a and b */
  for (i=0;i<n;++i){
    a[i] = i % 2;
    b[i] = -1;
  }
 
  ts = omp_get_wtime();
#pragma omp parallel for private(i) shared(a,b,n) reduction(+:dp) \
  schedule(static)
  for (i=0;i<n;++i)
    dp += a[i]*b[i];
 
  tf = omp_get_wtime();
  printf("dot product: %f, time(s):%f\n", dp,tf-ts);
}
